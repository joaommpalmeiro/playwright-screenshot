# tweets-via-twitter-publish

<div align="center">
  <img alt="" src="./screenshot.png" width="auto" height="512" title="Example" />
</div>

## Development

```bash
nvm install && nvm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

## References

- https://debbie.codes/blog/testing-iframes-with-playwright/
- https://cssify.appspot.com/
- https://github.com/microsoft/playwright/discussions/5068
- https://publish.twitter.com/
- https://github.com/privatenumber/snap-tweet
- https://twitter.com/imprensafalsa/status/1621497317298782210

## Notes

- https://playwright.dev/docs/api/class-locator#locator-get-attribute
- https://playwright.dev/docs/api/class-framelocator
- https://playwright.dev/docs/api/class-page#page-add-style-tag
- https://playwright.dev/docs/library
- https://playwright.dev/docs/locators#locate-by-css-or-xpath
- https://playwright.dev/docs/api/class-page#page-screenshot
- https://www.npmjs.com/package/link
- `npm install playwright && npm install -D typescript ts-node @tsconfig/node18 prettier @github/prettier-config`
- `sudo npx playwright install-deps` or `npx playwright install-deps`
- https://docs.gitlab.com/ee/user/markdown.html#change-the-image-or-video-dimensions
- https://docs.gitlab.com/ee/user/markdown.html#inline-html + https://github.com/gjtorikian/html-pipeline/blob/v2.12.3/lib/html/pipeline/sanitization_filter.rb#L42
