import {chromium, PageScreenshotOptions} from 'playwright'
import path from 'node:path'

// https://pawelgrzybek.com/typescript-interface-vs-type/#interfaces-are-restricted-to-an-object-type
type Lang = 'en' | 'pt'
type Theme = 'dark' | 'light'

const TWEET_URL: string = 'https://twitter.com/imprensafalsa/status/1621497317298782210'
const LANG: Lang = 'pt'
const THEME: Theme = 'dark'
const SCALE_FACTOR: number = 4

// Example: https://publish.twitter.com/?lang=pt&query=https%3A%2F%2Ftwitter.com%2Fimprensafalsa%2Fstatus%2F1621497317298782210&theme=light&widget=Tweet
const genPublishURL = (tweetURL: string, lang: Lang, theme: Theme): string => {
  const baseURL = 'https://publish.twitter.com/'
  const widgetParam = 'widget=Tweet'

  const langParam = `lang=${lang}`
  const themeParam = `theme=${theme}`

  const encodedTweetURL = encodeURIComponent(tweetURL)
  const queryParam = `query=${encodedTweetURL}`
  // console.log(queryParam)

  return `${baseURL}?${langParam}&${queryParam}&${themeParam}&${widgetParam}`
}

const main = async () => {
  // https://playwright.dev/docs/api/class-browser
  const browser = await chromium.launch()
  const page = await browser.newPage({
    deviceScaleFactor: SCALE_FACTOR,
  })

  const publishURL = genPublishURL(TWEET_URL, LANG, THEME)
  // console.log(publishURL)
  await page.goto(publishURL)

  // https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#type-assertions
  // <iframe>:
  const widgetURL = (await page.getByTitle('Twitter Tweet').getAttribute('src')) as string
  // console.log(widgetURL)

  // https://playwright.dev/docs/navigations#custom-wait
  // https://github.com/microsoft/playwright/issues/6046
  // Since sometimes the screenshot is taken without the images fully loading:
  await page.goto(widgetURL, {waitUntil: 'networkidle'})

  await page.addStyleTag({
    // path: './style.css',
    path: path.join(__dirname, 'style.css'),
  })

  const locator = page.locator('#app > div > div > div')
  const box = (await locator.boundingBox()) as PageScreenshotOptions['clip']
  // console.log(box)

  // https://playwright.dev/docs/api/class-locator#locator-screenshot
  // https://playwright.dev/docs/api/class-page#page-screenshot
  // Playwright rounds the height from 617.109375px to 618px (2472 / 4), hence the white border at the bottom of the image.
  // To avoid this, take the screenshot from the page (using the clip option) and not from the locator:
  // await locator.screenshot({
  await page.screenshot({
    path: 'screenshot.png',
    animations: 'disabled',
    omitBackground: true,
    clip: box,
  })

  await browser.close()
  console.log('All done!')
}

main()
